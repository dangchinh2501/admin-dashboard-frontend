import React from 'react'
import { Pagination } from 'react-admin'

const ListPagination = (props) => (
	<Pagination rowsPerPageOptions={[10, 20, 50, 100]} {...props} />
)

export default ListPagination
