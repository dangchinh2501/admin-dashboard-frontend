import React, { useState } from 'react'
import {
	Player,
	ControlBar,
	PlayToggle,
	VolumeMenuButton,
	PlaybackRateMenuButton,
	BigPlayButton,
} from 'video-react'
import HLSSource from './HLSSource'
import '../../node_modules/video-react/dist/video-react.css'
import { render } from '@testing-library/react'

export default class VideoPlayer extends React.Component {
	constructor(props, context) {
		super(props, context)

		this.state = {
			playerSource: `https://d1fysih6a5vern.cloudfront.net/${this.props.videoKey}`,
		}
	}

	render() {
		return (
			<Player fluid={false} width={500} height={300} playsInline>
				<HLSSource isVideoChild src={this.state.playerSource} />
				<BigPlayButton position="center" />
				<ControlBar autoHide={false} disableDefaultControls={true}>
					<PlayToggle />
					<VolumeMenuButton />
					<PlaybackRateMenuButton rates={[2, 1, 0.5, 0.1]} />
				</ControlBar>
			</Player>
		)
	}
}
