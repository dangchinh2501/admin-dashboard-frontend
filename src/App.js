import React from 'react'
import {
	Admin,
	Resource,
	ListGuesser,
	EditGuesser,
	ShowGuesser,
} from 'react-admin'
//import simpleProvider from 'ra-data-simple-rest'
import provider from './Common/DataProvider'
import { UserList } from './Resources/User/UserList'
import { UserEdit } from './Resources/User/UserEdit'
import { VideoDetailList } from './Resources/VideoDetail/VideoDetailList'
import { ReportVideoList } from './Resources/ReportVideo/ReportVideoList'
import { VideoDetailEdit } from './Resources/VideoDetail/VideoDetailEdit'
import { VideoDetailShow } from './Resources/VideoDetail/VideoDetailShow'

import Dashboard from './Dashboard'

function App() {
	return (
		<Admin
			dashboard={Dashboard}
			dataProvider={provider('http://localhost:4000')}
		>
			<Resource name="user" list={UserList} edit={UserEdit} />
			<Resource
				name="video-detail"
				list={VideoDetailList}
				edit={VideoDetailEdit}
				options={{ label: 'Video Details' }}
			/>
			<Resource
				name="report-video"
				list={ReportVideoList}
				options={{ label: 'Report Videos' }}
			/>
		</Admin>
	)
}

export default App
