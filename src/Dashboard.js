import * as React from 'react'
import { Card, CardContent, CardHeader } from '@material-ui/core'
import VideoPlayer from './Components/VideoPlayer'

export default () => (
	<Card>
		<CardHeader title="Welcome to the administration" />
		<CardContent>Lorem ipsum sic dolor amet...</CardContent>

		<VideoPlayer videoKey="output/hls/bc7cad3b8fce486c8cf87f8751d6bbb9.m3u8" />
	</Card>
)
