import React from 'react'
import {
	List,
	Datagrid,
	TextField,
	EmailField,
	NumberField,
	DateField,
	ReferenceField,
} from 'react-admin'
import ListPagination from '../../Common/ListPagination'

export const VideoDetailList = (props) => (
	<List {...props} pagination={<ListPagination />} title="Videos">
		<Datagrid rowClick="edit">
			<TextField source="id" />
			<ReferenceField source="userId" reference="user">
				<TextField source="username" />
			</ReferenceField>
			<TextField source="uploadTime" />
			<TextField source="latitude" />
			<TextField source="longitude" />
			<TextField source="isDeleted" />
			<TextField source="deletedBy" />
			<TextField source="deletedAt" />
		</Datagrid>
	</List>
)
