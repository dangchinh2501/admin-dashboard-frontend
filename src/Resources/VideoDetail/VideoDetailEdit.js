import React, { useState, useEffect } from 'react'
import {
	Edit,
	SimpleForm,
	TextInput,
	NumberInput,
	DateInput,
	ReferenceInput,
	SelectInput,
	useEditController,
} from 'react-admin'
import VideoPlayer from '../../Components/VideoPlayer'

export const VideoDetailEdit = (props) => {
	const { record = {} } = useEditController(props)
	const { videoKey } = record !== undefined && record

	return <VideoPlayer videoKey={videoKey} />
}
