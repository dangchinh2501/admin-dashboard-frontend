import React from 'react'
import { Show, SimpleShowLayout, TextField } from 'react-admin'

export const VideoDetailShow = (props) => {
	return (
		<Show {...props}>
			<SimpleShowLayout>
				<TextField source="id" />
				<TextField source="userId" />
				<TextField source="videoKey" />
				<TextField source="thumbnailKey" />
				<TextField source="caption" />
				<TextField source="latitude" />
				<TextField source="longitude" />
				<TextField source="loops" />
				<TextField source="uploadTime" />
			</SimpleShowLayout>
		</Show>
	)
}
