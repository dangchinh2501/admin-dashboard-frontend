import React from 'react'
import {
	List,
	Datagrid,
	TextField,
	EmailField,
	NumberField,
	DateField,
	ReferenceField,
} from 'react-admin'
import ListPagination from '../../Common/ListPagination'

export const ReportVideoList = (props) => (
	<List {...props} pagination={<ListPagination />} title="Report Videos">
		<Datagrid rowClick="edit">
			<TextField source="id" />
			<ReferenceField source="userId" reference="user">
				<TextField source="username" />
			</ReferenceField>
			<ReferenceField source="videoId" reference="video-detail">
				<TextField source="id" />
			</ReferenceField>
			<TextField source="type" />
			<TextField source="createdAt" />
		</Datagrid>
	</List>
)
