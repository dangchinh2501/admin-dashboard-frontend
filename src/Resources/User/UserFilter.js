import React from 'react'
import {
	Filter,
	ReferenceInput,
	SelectInput,
	TextInput,
	List,
} from 'react-admin'

export const UserFilter = (props) => (
	<Filter {...props}>
		<TextInput label="Search" source="username" alwaysOn />
		<ReferenceInput label="User" source="id" reference="user" allowEmpty>
			<SelectInput optionText="name" />
		</ReferenceInput>
	</Filter>
)
