import React from 'react'
import {
	Edit,
	SimpleForm,
	TextInput,
	NumberInput,
	DateInput,
	ReferenceInput,
	SelectInput,
	TextField,
} from 'react-admin'

export const UserEdit = (props) => (
	<Edit {...props}>
		<SimpleForm>
			<TextInput source="id" />
			<TextInput source="username" />
			<TextInput source="mobileNumber" />
			<TextInput source="fullName" />
			<TextInput source="email" />
			<TextInput source="password" />
			<NumberInput source="emailVerified" />
			<TextInput source="verificationToken" />
			<TextInput source="bio" />
			<DateInput source="dob" />
			<TextInput source="profilePic" />
			<DateInput source="createdAt" />
			<DateInput source="updatedAt" />
			<NumberInput source="isDeleted" />
			<TextInput source="deletedAt" />
			<TextInput source="deletedBy" />
			<NumberInput source="isPaused" />
		</SimpleForm>
	</Edit>
)
