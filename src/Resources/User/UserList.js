import React from 'react'
import {
	List,
	Datagrid,
	TextField,
	EmailField,
	NumberField,
	DateField,
} from 'react-admin'
import ListPagination from '../../Common/ListPagination'
import { UserFilter } from './UserFilter'

export const UserList = (props) => {
	return (
		<List
			{...props}
			pagination={<ListPagination />}
			filters={<UserFilter />}
		>
			<Datagrid {...props} rowClick="edit">
				<TextField source="id" />
				<TextField source="username" />
				<TextField source="mobileNumber" />
				<EmailField source="email" />
				<DateField source="createdAt" />
				<DateField source="updatedAt" />
				<NumberField source="isDeleted" />
				<TextField source="deletedAt" />
				<TextField source="deletedBy" />
				<NumberField source="isPaused" />
			</Datagrid>
		</List>
	)
}
